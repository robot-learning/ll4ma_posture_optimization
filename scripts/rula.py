#!/usr/bin/env python


import rospy
import numpy as np


from tf.transformations import euler_from_quaternion
from time import time
from std_msgs.msg import Int32, String


class Rula(object):
    """docstring for RULA."""
    def __init__(self, _joint_angles_rad, _task):
        super(Rula, self).__init__()
        self.joint_angles=[float(_joint_angles_rad[i])/np.pi*180 for i in range(len(_joint_angles_rad))]
        # print self.joint_angles
        self.task=_task
        #tables should be converted to global!!!!!
        self.table_A={
        "1111":1, "1112":2, "1121":2, "1122":2, "1131":2, "1132":3, "1141":3, "1142":3,
        "1211":2, "1212":2, "1221":2, "1222":2, "1231":3, "1232":3, "1241":3, "1242":3,
        "1311":2, "1312":3, "1321":3, "1322":3, "1331":3, "1332":3, "1341":4, "1342":4,

        "2111":2, "2112":3, "2121":3, "2122":3, "2131":3, "2132":4, "2141":4, "2142":4,
        "2211":3, "2212":3, "2221":3, "2222":3, "2231":3, "2232":4, "2241":4, "2242":4,
        "2311":3, "2312":4, "2321":4, "2322":4, "2331":4, "2332":4, "2341":5, "2342":5,

        "3111":3, "3112":3, "3121":4, "3122":4, "3131":4, "3132":4, "3141":5, "3142":5,
        "3211":3, "3212":4, "3221":4, "3222":4, "3231":4, "3232":4, "3241":5, "3242":5,
        "3311":4, "3312":4, "3321":4, "3322":4, "3331":4, "3332":5, "3341":5, "3342":5,

        "4111":4, "4112":4, "4121":4, "4122":4, "4131":4, "4132":5, "4141":5, "4142":5,
        "4211":4, "4212":4, "4221":4, "4222":4, "4231":4, "4232":5, "4241":5, "4242":5,
        "4311":4, "4312":4, "4321":4, "4322":5, "4331":5, "4332":5, "4341":6, "4342":6,

        "5111":5, "5112":5, "5121":5, "5122":5, "5131":5, "5132":6, "5141":6, "5142":7,
        "5211":5, "5212":6, "5221":6, "5222":6, "5231":6, "5232":7, "5241":7, "5242":7,
        "5311":6, "5312":6, "5321":6, "5322":7, "5331":7, "5332":7, "5341":7, "5342":8,

        "6111":7, "6112":7, "6121":7, "6122":7, "6131":7, "6132":8, "6141":8, "6142":9,
        "6211":8, "6212":8, "6221":8, "6222":8, "6231":8, "6232":9, "6241":9, "6242":9,
        "6311":9, "6312":9, "6321":9, "6322":9, "6331":9, "6332":9, "6341":9, "6342":9,
        }

        self.table_B={
        "111":1, "112":3, "121":2, "122":3, "131":3, "132":4, "141":5, "142":5, "151":6, "152":6, "161":7, "162":7,
        "211":2, "212":3, "221":2, "222":3, "231":4, "232":5, "241":5, "242":5, "251":6, "252":7, "261":7, "262":7,
        "311":3, "312":3, "321":3, "322":4, "331":4, "332":5, "341":5, "342":6, "351":6, "352":7, "361":7, "362":7,
        "411":5, "412":5, "421":5, "422":6, "431":6, "432":7, "441":7, "442":7, "451":7, "452":7, "461":8, "462":8,
        "511":7, "512":7, "521":7, "522":7, "531":7, "532":8, "541":8, "542":8, "551":8, "552":8, "561":8, "562":8,
        "611":8, "612":8, "621":8, "622":8, "631":8, "632":8, "641":8, "642":9, "651":9, "652":8, "661":9, "662":9,

        }

        #table C
        #key = "wrist/arm, neck/trunk/leg"
        # wrist/arm >8 --> =8
        # leg/trunk/neck >7 --> =7
        self.table_C={
        "11":1, "12":2, "13":3, "14":3, "15":4, "16":5, "17":5,
        "21":2, "22":2, "23":3, "24":4, "25":4, "26":5, "27":5,
        "31":3, "32":3, "33":3, "34":4, "35":4, "36":5, "37":6,
        "41":3, "42":3, "43":3, "44":4, "45":5, "46":6, "47":6,
        "51":4, "52":4, "53":4, "54":5, "55":6, "56":7, "57":7,
        "61":4, "62":4, "63":5, "64":6, "65":6, "66":7, "67":7,
        "71":5, "72":5, "73":6, "74":6, "75":7, "76":7, "77":7,
        "81":5, "82":5, "83":6, "84":7, "85":7, "86":7, "87":7,
        }

    def rula_score(self):

        self.upper_arm_index = 0
        self.lower_arm_index = 0
        self.wrist_index = 0
        self.wrist_twist_index = 0
        self.arm_muscle_use_score = 0
        self.arm_load_score = 0
        self.neck_score = 0
        self.trunk_score = 0
        self.leg_score = 0
        self.body_muscle_use_score = 0
        self.body_load_score = 0

        self.posture_score = 0
        self.neck_trunk_leg_score = 0
        self.RULA_score = 0


        # Upper Arm analysis#################
        self.upper_arm_analysis()
        self.lower_arm_analysis()
        self.wrist_analysis()

        #Find the posture score from table A
        table_a_key=str(self.upper_arm_index * 1000 + self.lower_arm_index * 100
        + self.wrist_index * 10 + self.wrist_twist_index)
        self.posture_score = self.table_A[table_a_key]
        # print "uai:"
        # print self.upper_arm_index
        # print "lai:"
        # print self.lower_arm_index
        # print "wi:"
        # print self.wrist_index
        # print "wti:"
        # print self.wrist_twist_index





        self.arm_muscle_use_anslysis()
        self.arm_load_analysis()
        # print "arm_score:"
        # print self.posture_score
        # print "armuse:"
        # print self.arm_muscle_use_score
        # print "armload:"
        # print self.arm_load_score

        #find row in table_C
        table_C_row=self.posture_score + self.arm_muscle_use_score + self.arm_load_score
        if table_C_row > 8:
                table_C_row = 8

        self.neck_analysis()
        self.trunk_analysis()
        self.leg_analysis()
        #
        # print "neck_score:"
        # print self.neck_score
        # print "trunk_score:"
        # print self.trunk_score
        # print "leg_score:"
        # print self.leg_score

        # find trunk_leg_neck score from table # BUG:
        table_B_key=str(self.neck_score * 100 + self.trunk_score * 10 + self.leg_score)
        self.neck_trunk_leg_score = self.table_B[table_B_key]
        # 
        # print 'neck_trunk_leg_score'
        # print self.neck_trunk_leg_score

        self.body_muscle_use_analysis()
        self.body_load_analysis()

        # print 'body_muscle_use_score'
        # print self.body_muscle_use_score
        # print 'body_load_score'
        # print self.body_load_score

        #find table_C column
        table_C_column=self.neck_trunk_leg_score + self.body_muscle_use_score + self.body_load_score
        if table_C_column > 7:
            table_C_column = 7

        #calculate the final RULA arm_score
        table_C_key = str(10 * table_C_row + table_C_column)
        self.RULA_score = self.table_C[table_C_key]

        self.analyze_RULA()

        # print "RULA SCORE = "
        # print self.RULA_score
        # print self.RULA_interpretion
        return self.RULA_score

    def upper_arm_analysis(self):

        if self.joint_angles[4]> -20 and self.joint_angles[4]<20:
            self.upper_arm_index +=1
        elif self.joint_angles[4]<=-20:
            self.upper_arm_index +=2
        elif self.joint_angles[4]>=20 and self.joint_angles[4]<45:
            self.upper_arm_index +=2
        elif self.joint_angles[4]>=45 and self.joint_angles[4]<90:
            self.upper_arm_index +=3
        elif self.joint_angles[4]>=90 and self.joint_angles[4]<180:
            self.upper_arm_index +=4
        else:
            print "Error in angle for theta_5"
        # Adjustment
        if self.task.raised_shoulder :
            self.upper_arm_index +=1
        if self.joint_angles[3]>= 5 : #upperarm is abducted
            self.upper_arm_index +=1
        if self.task.supported_arm or self.task.lean_on_arm:   #supported arm or learning
            self.upper_arm_index +=-1

    def lower_arm_analysis(self):

        if self.joint_angles[6]> 60 and self.joint_angles[6]<100:
            self.lower_arm_index +=1
        else:  #if self.joint_angles[6]> 100 or (self.joint_angles[6]>0 and self.joint_angles[6]<100) :
            self.lower_arm_index +=2

        # Adjustment
        # neeed to add arm_ee calculation
        if self.joint_angles[5]<-5 or self.task.arm_ee[1] < 0 : #if arm is working accross midline or to side of the body
            self.lower_arm_index +=1


    def wrist_analysis(self):

        if self.joint_angles[9]>-5 and self.joint_angles[9]< 5:
            self.wrist_index +=1
        elif self.joint_angles[9]>-15 and self.joint_angles[9]< 15:
            self.wrist_index +=3
        else: #if self.joint_angles[9]<=-15 or self.joint_angles[9]>= 15:
            self.wrist_index +=3
        # Adjustment
        if self.joint_angles[8]<-5 or self.joint_angles[8]>5:
                self.wrist_index +=1


        #wrist twist
        if self.joint_angles[7]>-30 and self.joint_angles[7]< 30: #?????? what is midrange?
            self.wrist_twist_index +=1
        else:
            self.wrist_twist_index +=2


    def arm_muscle_use_anslysis(self):
        if self.task.arm_motion=='dynamic' or self.task.arm_motion_freq >= 4: #held>10min
            self.arm_muscle_use_score +=1

    def arm_load_analysis(self):
        if self.task.arm_load < 2 and self.task.arm_load_type == 'intermittent': # (Kg)
            self.arm_load_score +=0
        elif self.task.arm_load < 2 and self.task.arm_load_type != 'intermittent':
            self.arm_load_score +=1
        elif self.task.arm_load >=2 and self.task.arm_load <10 and self.task.arm_load_type == 'intermittent':
            self.arm_lead_score +=1
        elif self.task.arm_load >=2 and self.task.arm_load <10 and self.task.arm_load_type != 'intermittent':
            self.arm_load_score +=2
        elif self.task.arm_load > 10 or self.task.arm_load_type=='repeated' or self.task.arm_load_type =='shock':
            self.arm_load_score +=3
        else:
            pass

    def neck_analysis(self):

        if self.task.neck_angle[0] >= 0 and self.task.neck_angle[0] < 10:
            self.neck_score +=1
        elif self.task.neck_angle[0] >= 10 and self.task.neck_angle[0] < 20:
            self.neck_score +=2
        elif self.task.neck_angle[0] >= 20:
            self.neck_score +=3
        else: #neck in extention backward
            self.neck_score +=4
        # Adjustment
        if self.task.neck_angle[2]>5 or self.task.neck_angle[2]< -5:  #if neck is twisted
            self.neck_score +=1
        if self.task.neck_angle[1]>5 or self.task.neck_angle[1]< -5:  #if neck is side_bended
            self.neck_score +=1

    def trunk_analysis(self):

        if self.joint_angles[0] < 5:
            self.trunk_score += 1
        elif self.joint_angles[0] > 5 and self.joint_angles[0] < 20:
                self.trunk_score += 2
        elif self.joint_angles[0] >= 20 and self.joint_angles[0] < 60:
                self.trunk_score += 3
        elif self.joint_angles[0] >= 60:
                self.trunk_score += 4
        else:
             pass
        # Adjustment
        if self.joint_angles[1] > 5 or self.joint_angles[1] < -5:
                self.trunk_score += 1
        if self.joint_angles[2] > 5 or self.joint_angles[2] < -5:
                self.trunk_score += 1

    def leg_analysis(self):

        if self.task.supported_leg_feet:
            self.leg_score +=1
        else:
            self.leg_score +=2

    def body_muscle_use_analysis(self):
        if self.task.body_motion == 'dynamic' or self.task.body_motion_freq >= 4: #held>10min
            self.body_muscle_use_score +=1

    def body_load_analysis(self):
        if self.task.body_load < 2 and self.task.body_load_type == 'intermittent': # (Kg)
            self.body_load_score +=0
        elif self.task.body_load < 2 and self.task.body_load_type != 'intermittent':
            self.body_load_score +=1
        elif self.task.body_load >=2 and self.task.body_load <10 and self.task.body_load_type == 'intermittent':
            self.body_lead_score +=1
        elif self.task.body_load >=2 and self.task.body_load <10 and self.task.body_load_type != 'intermittent':
            self.body_load_score +=2
        elif self.task.body_load > 10 or self.task.body_load_type=='repeated' or self.task.body_load_type =='shock':
            self.body_load_score +=3
        else:
            pass

    def analyze_RULA(self):
        if self.RULA_score == 1 or self.RULA_score == 1:
            self.RULA_interpretion = 'Acceptable posture'
        elif self.RULA_score == 3  or self.RULA_score == 4:
            self.RULA_interpretion = 'Further investigation, change may be needed'
        elif self.RULA_score == 5 or self.RULA_score == 6:
            self.RULA_interpretion = 'Further investigation, change soon'
        elif self.RULA_score == 7:
            self.RULA_interpretion = 'Investigate and implement change'
        else:
            self.RULA_interpretion = 'Wrong RULA score'





class Task(object):
    """docstring for RULA."""
    def __init__(self):
        super(Task, self).__init__()
        self.raised_shoulder = False
        self.supported_arm = False
        self.lean_on_arm = False
        self.arm_motion = 'dynamic'  #or 'dynamic'
        self.arm_motion_freq = 3 #frequency (occurance/min)
        self.arm_load = 0.1 # max load on arm in (kg)
        self.arm_load_type = 'intermittent'  # or 'repeated' or 'shock'
        self.neck_angle = [0,0,0]  #we do not have neck angle as a part of posture in our model so we define it in the task
        self.supported_leg_feet = True
        self.body_motion = 'static'  #or 'dynamic'
        self.body_motion_freq = 3 #frequency (occurance/min)
        self.body_load = 1 # max load on body in (kg)
        self.body_load_type = 'intermittent'  # or 'repeated' or 'shock'
        self.arm_ee=[.5,0.2,1]


#
# if __name__ == '__main__':
#
#     try:
#
#         posture=[0, 0, 0, 0, 1, 0, 0, 3, 0, 0]
#         task1=task()
#         # task1.raised_shoulder = False
#         # task1.supported_arm = False
#         # task1.lean_on_arm = False
#         # task1.arm_motion = 'static'  #or 'dynamic'
#         # task1.arm_motion_freq = 3 #frequency (occurance/min)
#         # task1.arm_load = 1 # max load on arm in (kg)
#         # task1.arm_load_type = 'intermittent'  # or 'repeated' or 'shock'
#         # task1.neck_angle = [0,0,0]  #we do not have neck angle as a part of posture in our model so we define it in the task
#         # task1.supported_leg_feet = False
#         # task1.body_motion = 'static'  #or 'dynamic'
#         # task1.body_motion_freq = 3 #frequency (occurance/min)
#         # task1.body_load = 1 # max load on body in (kg)
#         # task1.body_load_type = 'intermittent'  # or 'repeated' or 'shock'
#
#         RULA_1= RULA(posture,task1)
#         RULA_1.rula_score()
#
#
#
#
#
#
#         # rospy.spin()
#     except rospy.ROSInterruptException:
#         pass
